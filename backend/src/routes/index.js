const { Router } = require("express");

const router = Router();

const User = require("../models/User");

const jwt = require("jsonwebtoken");

router.get("/", (req, res) => res.send("Hello world!"));

router.post("/signup", async (req, res) => {
  const { email, password } = req.body;
  const newUser = new User(
    {
      _id: undefined,
      email: email,
      password: password
    },
    {
      timestamps: true
    }
  );
  await newUser.save();

  const token = jwt.sign({ _id: newUser._id }, "secretKey");
  res.status(200).json({ token });
});

router.post("/signin", async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  if (!user) return res.status(401).send("correo no existe");
  if (user.password !== password) res.status(401).send("contraseña erronea");

  const token = jwt.sign({ _id: user._id }, "secretKey");
  return res.status(200).json({ token });
});

router.get("/tasks", (req, res) => {
  res.json([
    {
      _id: 1,
      name: "Task one",
      description: "lorem ipsum",
      date: "26-11-2019"
    },
    {
      _id: 2,
      name: "Task two",
      description: "lorem ipsum",
      date: "26-11-2019"
    },
    {
      _id: 3,
      name: "Task three",
      description: "lorem ipsum",
      date: "26-11-2019"
    }
  ]);
});

router.get("/privateTasks",verify, (req, res) => {
  res.json([
    {
      _id: 1,
      name: "Task one",
      description: "lorem ipsum",
      date: "26-11-2019"
    },
    {
      _id: 2,
      name: "Task two",
      description: "lorem ipsum",
      date: "26-11-2019"
    },
    {
      _id: 3,
      name: "Task three",
      description: "lorem ipsum",
      date: "26-11-2019"
    }
  ]);
});

module.exports = router;

function verify(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send("sin autorización");
  }
  const token = req.headers.authorization.split(" ")[1];


  if (token == null){
    return res.status(401).send("sin autorización");
  } 
  const payload = jwt.verify(token, "secretKey");
  req.userId = payload._id;
  next();
}
